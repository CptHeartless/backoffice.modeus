/**
 * Stylesheet
 */
require('../stylesheets/backoffice.scss');

/**
 * Modules
 */
require('./modules/boCarousel/');
require('./modules/boEditable/');
require('./modules/modal/');
require('./modules/boApi/');

/**
 * Shared
 */
const routerHelperProvider = require('./shared/routerHelper.provider');
const onEnterDirective = require('./shared/onEnter.directive');
const clickOutsideDirective = require('./shared/clickOutside.directive');

/**
 * Config file
 */
const config = ENV === 'production' ? require('./config.prod') : require('./config.dev');

function getStates() {
  return config.routes;
}

function appRun(routerHelper) {
  routerHelper.configureStates(getStates());
}
appRun.$inject = ['routerHelper'];

angular
  .module('backoffice', ['ui.router', 'ui-notification', 'mm.foundation', 'ngAnimate', 'bo.carousel',
    'ngDraggable', 'bo.editable', 'bo.modal', 'bo.api'])
    .constant('$config', config)
    .provider('routerHelper', routerHelperProvider)
    .directive('clickOutside', clickOutsideDirective)
    .directive('onEnter', onEnterDirective)
  .run(appRun);


/**
 * Components
 */
require('./components/access/');
require('./components/members/');
require('./components/teams');
require('./components/users/');
require('./components/user/');
require('./components/addMembers/');
require('./components/scenarios/');
require('./components/games/');
require('./components/game/');
