function modalController($attrs, $scope) {
  const vm = this;
  const sizes = ['tiny', 'small', 'large', 'full'];

  function hideModal() {
    document.body.style.overflow = '';
    vm.hide();
  }

  function getModalClasses() {
    const modalClasses = {
      _fade: vm.show,
      _in: vm.show,
    };
    if (sizes.includes($attrs.revealSize)) modalClasses[`_${$attrs.revealSize}`] = true;
    return modalClasses;
  }

  function onModalShowChange() {
    if (vm.show) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = '';
    }
  }

  const modalShowUnwatch = $scope.$watch('modalVm.show', onModalShowChange);

  function onDestroy() {
    modalShowUnwatch();
  }

  $scope.$on('$destroy', onDestroy);

  vm.getModalClasses = getModalClasses;
  vm.hideModal = hideModal;
}

modalController.$inject = ['$attrs', '$scope'];

module.exports = modalController;
