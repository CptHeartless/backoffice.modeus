const modalTemplate = require('./modal.view.pug');
const modalController = require('./modal.controller.js');

function modalDirective() {
  const directive = {
    template: modalTemplate,
    transclude: true,
    restrict: 'EA',
    scope: { show: '=', revealSize: '=', hide: '&' },
    replace: true,
    controller: modalController,
    controllerAs: 'modalVm',
    bindToController: true,
  };
  return directive;
}

module.exports = modalDirective;
