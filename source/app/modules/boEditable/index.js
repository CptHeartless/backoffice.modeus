const boEditableDirective = require('./boEditable.directive');

angular.module('bo.editable', [])
  .directive('boEditable', boEditableDirective);
