const boApiFactory = require('./boApi.factory');

angular.module('bo.api', [])
  .factory('$api', boApiFactory);
