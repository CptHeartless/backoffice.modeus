const Api = require('../../helpers/portal.api');

function boApiFactory($config, Notification) {
  const apiConfig = Object.assign({
    onUncauchError(error) {
      Notification.error(error.message);
    },
  }, $config.apiOptions);

  const api = new Api(apiConfig);

  return api;
}

boApiFactory.$inject = ['$config', 'Notification'];

module.exports = boApiFactory;
