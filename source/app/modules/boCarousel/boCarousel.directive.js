const boCarouselTemplate = require('./boCarousel.view.pug');
const boCarouselController = require('./boCarousel.controller');

function boCarouselDirective() {
  const directive = {
    template: boCarouselTemplate,
    transclude: true,
    restrict: 'EA',
    replace: true,
    controller: boCarouselController,
    controllerAs: 'carouselVm',
    bindToController: true,
  };

  return directive;
}

module.exports = boCarouselDirective;
