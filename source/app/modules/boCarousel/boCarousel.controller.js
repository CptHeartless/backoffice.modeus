function boCarouselController($scope, $element, $log) {
  const vm = this;
  vm.$boCarousel = $element[0];
  vm.$boCarouselWrapper = $element.children()[0];
  vm.carouselWidth = 0;
  vm.wrapperWidth = 0;
  vm.offset = 0;
  vm.$controlButtons =
    angular.element(document.querySelectorAll(`a[href="#${vm.$boCarousel.id}"]`));
  if (vm.$controlButtons.length < 2) {
    $log.error('Control button less then 2 or does not exist');
  }

  function canSlide() {
    return vm.carouselWidth < vm.wrapperWidth;
  }

  function renderOffset() {
    vm.$boCarouselWrapper.style.transform = `translateX(${vm.offset}px)`;
  }

  function onCarouselResize() {
    $element.css('width', '100%');
    vm.carouselWidth = vm.$boCarousel.offsetWidth;
    const wrapperWidth = vm.$boCarouselWrapper.offsetWidth;
    if (wrapperWidth < vm.wrapperWidth && vm.offset < 0) {
      const deltaWidth = vm.wrapperWidth - wrapperWidth;
      vm.offset += deltaWidth;
      renderOffset();
    }
    vm.wrapperWidth = wrapperWidth;
    if (canSlide()) {
      vm.$controlButtons.css('opacity', '1');
    } else {
      vm.$controlButtons.css('opacity', '0');
    }
  }

  function slideRight() {
    if (!canSlide()) return;
    let freeArea = (vm.wrapperWidth - vm.carouselWidth) + vm.offset;
    if (freeArea <= 0) return;
    if (freeArea > vm.carouselWidth) freeArea = vm.carouselWidth;
    vm.offset -= freeArea;
    renderOffset();
  }

  function slideLeft() {
    if (vm.offset >= 0 && !canSlide()) return;
    let freeArea = Math.abs(vm.offset);
    if (freeArea > vm.carouselWidth) freeArea = vm.carouselWidth;
    vm.offset += freeArea;
    renderOffset();
  }

  function onControlButtonClick(event) {
    const direction = Number(event.currentTarget.dataset.direction);

    if (direction + 1) {
      slideRight();
    } else {
      slideLeft();
    }

    event.preventDefault();
  }

  const wrapperWidthUnwatch = $scope.$watch('carouselVm.$boCarouselWrapper.offsetWidth', onCarouselResize);

  vm.$controlButtons.bind('click', onControlButtonClick);

  function onDestroy() {
    wrapperWidthUnwatch();
    vm.$controlButtons.unbind('click', onControlButtonClick);
  }

  $scope.$on('$destroy', onDestroy);

  vm.slideRight = slideRight;
  vm.slideLeft = slideLeft;
}

boCarouselController.$inject = ['$scope', '$element', '$document', '$log'];

module.exports = boCarouselController;
