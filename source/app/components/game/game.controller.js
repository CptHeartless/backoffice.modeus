function gameController(teams, $stateParams, games, users, $state, scenarios, $scope) {
  const vm = this;
  vm.gameRaw = {};

  let creator = {};
  vm.scenarios = [];
  vm.teams = [];
  vm.startProgress = 0;
  vm.isCalculating = false;
  let scenario = {};

  if (vm.gameRaw) creator = {};

  function generateLifecycle() {
    const lifecycle = [];
    const lifecycleTypes = [
      { id: 'past', name: 'Прошлые' },
      { id: 'present', name: 'Текущий' },
      { id: 'present-future', name: 'Следующий' },
      { id: 'future', name: 'Будущие' },
    ];
    let prevStepType = null;
    vm.gameRaw.steps.forEach((step) => {
      let stepType = 1;
      let progress = 0;
      if (step.period < vm.gameRaw.period) {
        progress = 100;
        stepType = 0;
      }
      if (step.period > vm.gameRaw.period) stepType = 3;
      if (step.period === vm.gameRaw.period + 1 && !vm.gameRaw.finished) stepType = 2;
      const { period, name } = step;
      lifecycle.push({ period, name, progress });
      _.last(lifecycle).type = lifecycleTypes[stepType];
      if (prevStepType !== stepType) {
        _.last(lifecycle).first = true;
        prevStepType = stepType;
      }
    });

    return lifecycle;
  }

  vm.currentSetup = '';

  function $apply(fn) {
    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
      $scope.$apply(fn);
    }
  }

  function $digest() {
    if ($scope.$root.$$phase !== '$apply' && $scope.$root.$$phase !== '$digest') {
      $scope.$digest();
    }
  }

  function updateLifecycle() {
    vm.game.lifecycle = generateLifecycle();
  }

  function showSetup(setup) {
    vm.currentSetup = vm.currentSetup === setup ? '' : setup;
  }

  function changeScenario(newScenario) {
    games.changeScenario(vm.gameRaw, newScenario.id).then(() => {
      vm.game.scenario = newScenario;
      vm.game.scenarioId = newScenario.id;
      $apply();
    });
  }

  function hasScenario() {
    return !!vm.game && !!vm.game.scenario;
  }

  function isFormedTeams() {
    return vm.teams.length > 1 && vm.teams.some(team => team.membersIds.length);
  }

  function canCalc() {
    return hasScenario() && isFormedTeams();
  }

  function startCalc(period = vm.game.period + 1) {
    if (!vm.canCalc || vm.isCalculating || period <= 0) return;
    vm.isCalculating = true;
    function onProgressChange(progress) {
      const currrentStep = vm.game.lifecycle.find(step => step.period === vm.game.period);
      if (currrentStep) {
        currrentStep.progress = progress;
      } else {
        vm.startProgress = progress;
      }
      $apply();
    }
    games.calc(vm.gameRaw.id, period, this::onProgressChange).then(() => {
      vm.game.period = vm.gameRaw.period;
      updateLifecycle();
      vm.isCalculating = false;
      $apply();
    }).catch(() => {
      vm.isCalculating = false;
    });
  }

  function toggleInterface() {
    if (vm.game.period < 1) return;
    games.toggleInterface(vm.gameRaw).then(() => {
      vm.game.interface = vm.gameRaw.interface;
      $digest();
    });
  }

  function finishGame() {
    games.finish(vm.gameRaw).then(() => {
      vm.game.finished = true;
      updateLifecycle();
      $digest();
    });
  }

  function resumeGame() {
    games.resume(vm.gameRaw).then(() => {
      vm.game.finished = false;
      updateLifecycle();
      $digest();
    });
  }

  function changeName(name) {
    games.changeName(vm.gameRaw, name).then(() => {
      vm.game.name = name;
    });
  }

  function changeDescription(description) {
    games.changeDescription(vm.gameRaw, description).then(() => {
      vm.game.description = description;
      $digest();
    });
  }

  function onTeamsLengthChange() {
    vm.teams = (teams.getAll() || []).length > 0 ? teams.getByIds(vm.gameRaw.teamsIds) : [];
  }

  function fillGame() {
    vm.gameRaw = games.getById($stateParams.gameId);
    if (!vm.gameRaw) $state.go('backoffice');
    vm.scenarios = scenarios.getAll();
    scenario = scenarios.getById(vm.gameRaw.scenarioId);
    creator = users.getById(vm.gameRaw.creatorId);
    vm.game = Object.assign({ creator, scenario, lifecycle: generateLifecycle() },
      _.cloneDeep(vm.gameRaw));
    onTeamsLengthChange();
  }

  function resetGame(keepScenario = false, keepTeams = false, calcOnReset = false) {
    const gameTeams = vm.gameRaw.teamsIds.slice();
    games.reset(vm.gameRaw, keepScenario, keepTeams).then(() => {
      if (!keepScenario) {
        vm.game.scenario = undefined;
        vm.game.scenarioId = undefined;
      }
      if (!keepTeams) {
        $scope.$broadcast('game:teams::reset');
        vm.game.teamsIds = [];
        teams.getByIds(gameTeams).forEach((team) => {
          teams.getAll().splice(this.teams.indexOf(team), 1);
        });
      }
      vm.game.finished = false;
      vm.game.period = 0;
      updateLifecycle();
      $apply(calcOnReset && keepScenario && keepTeams ? vm.startCalc : angular.noop);
    });
  }

  function fork() {
    if (vm.game.period < 1) return;
    games.fork(vm.game.id).then((forked) => {
      teams.getAll().push(...forked.teams);
      $state.go('game', { gameId: forked.game.id });
    });
  }

  Promise.all([
    teams.recievedTeams,
    scenarios.recievedScenarios,
    games.recievedGames,
    users.recievedUsers,
  ]).then(() => {
    fillGame();
    $apply();
  });

  $scope.$watch('vm.gameRaw.teamsIds.length', onTeamsLengthChange);

  vm.fork = fork;
  vm.resetGame = resetGame;
  vm.changeDescription = changeDescription;
  vm.changeName = changeName;
  vm.resumeGame = resumeGame;
  vm.finishGame = finishGame;
  vm.toggleInterface = toggleInterface;
  vm.startCalc = startCalc;
  vm.canCalc = canCalc;
  vm.isFormedTeams = isFormedTeams;
  vm.hasScenario = hasScenario;
  vm.showSetup = showSetup;
  vm.changeScenario = changeScenario;
}

gameController.$inject = ['teams', '$stateParams', 'games', 'users', '$state',
  'scenarios', '$scope'];

module.exports = gameController;
