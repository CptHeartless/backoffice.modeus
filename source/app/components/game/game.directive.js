const gameTemplate = require('./game.view.pug');
const gameController = require('./game.controller');

function gameDirective() {
  const directive = {
    template: gameTemplate,
    restrict: 'EA',
    replace: true,
    controller: gameController,
    controllerAs: 'vm',
    bindToController: true,
  };
  return directive;
}

module.exports = gameDirective;
