const gameDirective = require('./game.directive');

angular.module('backoffice')
  .directive('game', gameDirective);
