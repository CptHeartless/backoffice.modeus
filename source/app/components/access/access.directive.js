const accessTemplate = require('./access.view.pug');
const accessController = require('./access.controller.js');

function accessDirective() {
  const directive = {
    template: accessTemplate,
    restrict: 'EA',
    replace: true,
    controller: accessController,
    controllerAs: 'accessVm',
    bindToController: true,
  };
  return directive;
}

module.exports = accessDirective;
