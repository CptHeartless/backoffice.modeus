const accessDirective = require('./access.directive');

angular.module('backoffice')
  .directive('access', accessDirective);
