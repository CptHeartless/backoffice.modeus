const addMembersTemplate = require('./addMembers.view.pug');
const addMembersController = require('./addMembers.controller');

function addMembersDirective() {
  const directive = {
    restrict: 'EA',
    template: addMembersTemplate,
    replace: true,
    scope: { gameId: '=' },
    controller: addMembersController,
    controllerAs: 'addMembersVm',
    bindToController: true,
  };

  return directive;
}

module.exports = addMembersDirective;
