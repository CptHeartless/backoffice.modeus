function gameService(users, $api) {
  this.games = [];
  this.recievedGames = $api.getGames().then((games) => {
    this.games = games;
    return games;
  });

  function getAll() {
    return this.games;
  }

  function getById(gameId) {
    return this.games.filter(game => game.id === gameId)[0];
  }

  function addTeam(game, teamId) {
    if (!this.games.includes(game)) return Promise.reject(new Error('Game does not exist'));
    if (this.games.includes(teamId)) return Promise.reject(new Error('Team is exist'));
    game.teamsIds.push(teamId);
    return Promise.resolve(game.id);
  }

  function addTeamById(gameId, teamId) {
    const game = this.getById(gameId);
    return this.addTeam(game, teamId);
  }

  function deleteById(id) {
    const game = this.getById(id);
    return $api.deleteGame(id).then(() => {
      this.games.splice(this.games.indexOf(game), 1);
      return true;
    });
  }

  function create(name) {
    return $api.createGame(name).then((createdGame) => {
      this.games.push(createdGame);
      return createdGame.id;
    });
  }

  function fork(id) {
    return $api.forkGame(id).then((forked) => {
      this.games.push(forked.game);
      return forked;
    });
  }

  function addMember(game, userId) {
    if (!this.games.includes(game)) return Promise.reject(false);
    return $api.changeGameMembers(game.id, {
      add: [userId],
    }).then(() => {
      if (!game.membersIds.includes(userId)) game.membersIds.push(userId);
      const user = users.getById(userId);
      if (user.master && !game.mastersIds.includes(userId)) {
        game.mastersIds.push(userId);
      }
      return true;
    });
  }

  function removeMember(game, userId) {
    if (!this.games.includes(game)) return Promise.reject(false);
    return $api.changeGameMembers(game.id, {
      remove: [userId],
    }).then(() => {
      if (game.membersIds.includes(userId)) {
        game.membersIds.splice(game.membersIds.indexOf(userId), 1);
      }
      if (game.mastersIds.includes(userId)) {
        game.mastersIds.splice(game.mastersIds.indexOf(userId), 1);
      }
      return true;
    });
  }

  function changeScenario(game, scenarioId) {
    if (!this.games.includes(game)) return Promise.reject(false);
    return $api.changeGameScenario(game.id, scenarioId).then(() => {
      Object.assign(game, { scenarioId });
      return true;
    });
  }

  function calc(gameId, period, callback) {
    function onProgressChange(progress) {
      callback(progress);
    }

    return $api.calculateGame(gameId, period).then((response) => {
      onProgressChange(response.progress);

      return response.progress >= 100 ? response.progress
        : this.calc(gameId, period, callback);
    }).then(() => {
      const game = this.getById(gameId);
      game.period = period;

      return true;
    });
  }

  function toggleInterface(game) {
    if (!this.games.includes(game)) return Promise.reject(false);
    const gameInterface = !game.interface;
    return $api.modifyGameInterface(game.id, gameInterface).then(() => {
      Object.assign(game, { interface: gameInterface });
      return true;
    });
  }

  function finish(game) {
    if (!this.games.includes(game)) return Promise.reject(false);
    return $api.finishGame(game.id, true).then(() => {
      Object.assign(game, { finished: true });
      return true;
    });
  }

  function resume(game) {
    if (!this.games.includes(game)) return Promise.reject(false);
    return $api.finishGame(game.id, false).then(() => {
      Object.assign(game, { finished: false });
      return true;
    });
  }

  function changeName(game, name) {
    if (!this.games.includes(game)
        || String(name).trim().length === 0) return Promise.reject(false);
    return $api.changeGameName(game.id, name).then(() => {
      Object.assign(game, { name });
      return true;
    });
  }

  function changeDescription(game, description) {
    if (!this.games.includes(game)) return Promise.reject(false);
    return $api.changeGameDescription(game.id, description).then(() => {
      Object.assign(game, { description });
      return true;
    });
  }

  function reset(game, keepScenario = false, keepTeams = false) {
    if (!this.games.includes(game)) return Promise.reject(false);
    return $api.resetGame(game.id, keepScenario, keepTeams).then(() => {
      Object.assign(game, {
        period: 0,
        scenarioId: keepScenario ? game.scenarioId : 0,
        teamsIds: keepTeams ? game.teamsIds : [],
        finished: false,
      });

      return true;
    });
  }

  this.fork = fork;
  this.reset = reset;
  this.changeDescription = changeDescription;
  this.changeName = changeName;
  this.finish = finish;
  this.resume = resume;
  this.toggleInterface = toggleInterface;
  this.calc = calc;
  this.changeScenario = changeScenario;
  this.addMember = addMember;
  this.addTeam = addTeam;
  this.removeMember = removeMember;
  this.addTeamById = addTeamById;
  this.deleteById = deleteById;
  this.create = create;
  this.getById = getById;
  this.getAll = getAll;
}

gameService.$inject = ['users', '$api'];

module.exports = gameService;
