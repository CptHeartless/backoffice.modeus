const gamesTemplate = require('./games.view.pug');
const gamesController = require('./games.controller.js');

function gamesDirective() {
  const directive = {
    template: gamesTemplate,
    restrict: 'EA',
    replace: true,
    controller: gamesController,
    controllerAs: 'vm',
    bindToController: true,
  };
  return directive;
}

module.exports = gamesDirective;
