const teamsService = require('./teams.service');
const teamsDirective = require('./teams.directive');

angular.module('backoffice')
  .service('teams', teamsService)
  .directive('teams', teamsDirective);
