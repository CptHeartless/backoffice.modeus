function teamsService(games, $api) {
  this.teams = [];
  this.recievedTeams = $api.getTeams().then((teams) => {
    this.teams = teams;
    return teams;
  });

  function getAll() {
    return this.teams;
  }

  function getById(id) {
    return this.teams.filter(team => team.id === id)[0];
  }

  function getByIds(ids) {
    const teams = [];
    ids.forEach((teamId) => {
      teams.push(this.getById(teamId));
    });

    return teams;
  }

  function create(gameId) {
    const game = games.getById(gameId);
    return $api.createGameTeam(gameId).then((team) => {
      this.teams.push(team);
      return games.addTeam(game, team.id).then(() => team);
    });
  }

  function deleteById(teamId, game) {
    const team = this.getById(teamId);
    return $api.deleteGameTeam(game.id, team.id).then(() => {
      this.teams.splice(this.teams.indexOf(team), 1);
      game.teamsIds.splice(game.teamsIds.indexOf(teamId), 1);
      return true;
    });
  }

  function removeMemberWithoutApi(team, memberId) {
    if (!this.teams.includes(team)) return Promise.reject(new Error('Teams is not exist'));
    if (!team.membersIds.includes(memberId)) return Promise.reject(new Error('Member is not exist'));
    team.membersIds.splice(team.membersIds.indexOf(memberId), 1);
    return Promise.resolve(true);
  }

  function removeMember(gameId, team, memberId) {
    return $api.changeTeamMembers(gameId, team.id, {
      remove: [memberId],
    }).then(() => this.removeMemberWithoutApi(team, memberId));
  }

  function addMemberById(gameId, teamId, memberId) {
    const team = this.getById(teamId);
    return $api.changeTeamMembers(gameId, teamId, {
      add: [memberId],
    }).then(() => {
      if (_.isUndefined(team)) return Promise.reject(new Error('Team is not exist'));
      if (team.membersIds.includes(memberId)) return Promise.reject(new Error('Member is exist'));
      team.membersIds.push(memberId);
      return true;
    });
  }

  this.removeMemberWithoutApi = removeMemberWithoutApi;
  this.addMemberById = addMemberById;
  this.removeMember = removeMember;
  this.deleteById = deleteById;
  this.create = create;
  this.getByIds = getByIds;
  this.getById = getById;
  this.getAll = getAll;
}

teamsService.$inject = ['games', '$api'];

module.exports = teamsService;
