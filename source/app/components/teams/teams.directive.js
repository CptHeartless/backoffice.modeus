const teamsTemplate = require('./teams.view.pug');
const teamsController = require('./teams.controller');

function teamsDirective() {
  const directive = {
    template: teamsTemplate,
    restrict: 'EA',
    replace: true,
    controller: teamsController,
    controllerAs: 'teamsVm',
    bindToController: true,
  };
  return directive;
}

module.exports = teamsDirective;
