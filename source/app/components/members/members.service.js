function membersService() {
  this.members = [
    {
      gameId: 0,
      users: [],
    },
  ];

  function getAll() {
    return this.members;
  }

  this.getAll = getAll;
}

module.exports = membersService;
