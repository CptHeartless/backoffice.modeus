const membersTemplate = require('./members.view.pug');
const membersController = require('./members.controller');

function membersDirective() {
  const directive = {
    template: membersTemplate,
    restrict: 'EA',
    replace: true,
    scope: { unassigned: '=' },
    controller: membersController,
    controllerAs: 'membersVm',
    bindToController: true,
  };
  return directive;
}

module.exports = membersDirective;
