const membersDirective = require('./members.directive');

angular.module('backoffice')
  .directive('members', membersDirective);
