const userDirective = require('./user.directive');

angular.module('backoffice')
  .directive('user', userDirective);
