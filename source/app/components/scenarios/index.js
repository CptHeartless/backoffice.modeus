const scenariosService = require('./scenarios.service');

angular.module('backoffice')
  .service('scenarios', scenariosService);
