function scenariosService($api) {
  this.scenarios = [];
  this.recievedScenarios = $api.getScenarios().then((scenarios) => {
    this.scenarios = scenarios;
    return scenarios;
  });

  function getAll() {
    return this.scenarios;
  }

  function getById(id) {
    const scenarioId = Number(id);
    return this.scenarios.filter(scenario => scenario.id === scenarioId)[0];
  }

  this.getAll = getAll;
  this.getById = getById;
}

scenariosService.$inject = ['$api'];

module.exports = scenariosService;
