const usersFilter = require('./users.filter');
const usersService = require('./users.service');
const usersDirective = require('./users.directive');

angular.module('backoffice')
  .filter('usersFilter', usersFilter)
  .service('users', usersService)
  .directive('users', usersDirective);
