function usersController(users) {
  const vm = this;
  vm.users = users.getAll();
}

usersController.$inject = ['users'];

module.exports = usersController;
