function usersService($api) {
  this.users = [];
  this.recievedUsers = $api.getUsers().then((users) => {
    this.users = users;
    return users;
  });

  this.statuses = ['Администратор', 'Разработчик', 'Игрок'];
  this.permissions = [];

  function getById(id) {
    return this.users.filter(user => user.id === id)[0];
  }

  function getByIds(ids) {
    if (ids.length === 0) return [];
    const users = [];
    ids.forEach((id) => {
      users.push(this.getById(id));
    });
    return users;
  }

  function getAll() {
    return this.users;
  }

  this.getById = getById;
  this.getByIds = getByIds;
  this.getAll = getAll;
}

usersService.$inject = ['$api'];

module.exports = usersService;
