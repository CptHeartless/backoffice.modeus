const usersTemplate = require('./users.view.pug');
const usersController = require('./users.controller.js');

function usersDirective() {
  const directive = {
    template: usersTemplate,
    restrict: 'EA',
    replace: true,
    controller: usersController,
    controllerAs: 'usersVm',
    bindToController: true,
  };
  return directive;
}

module.exports = usersDirective;
