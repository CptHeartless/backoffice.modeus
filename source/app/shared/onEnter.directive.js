function onEnterDirective() {
  return ($scope, $element, $attrs) => {
    function onKeyUp(event) {
      if (event.which === 13) {
        $scope.$apply(() => {
          $scope.$eval($attrs.onEnter);
        });
      }
    }

    $element.bind('keyup', onKeyUp);

    function onDestroy() {
      $element.unbind('keyup', onKeyUp);
    }

    $scope.$on('$destory', onDestroy);
  };
}

module.exports = onEnterDirective;
