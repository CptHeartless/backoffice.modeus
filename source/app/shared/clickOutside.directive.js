function clickOutside($document) {
  function clickOutsideLinkPost($scope, $element, $attrs) {
    function onClick(event) {
      const isChild = _.some($element.children(), el => el === event.target);
      const isSelf = $element[0] === event.target;
      const isInside = isChild || isSelf;
      if (!isInside) {
        $scope.$apply($attrs.clickOutside);
      }
    }
    $document.bind('click', onClick);

    function onDestroy() {
      $document.unbind('click', onClick);
    }

    $scope.$on('$destroy', onDestroy);
  }

  clickOutsideLinkPost.$inject = ['$scope', '$element', '$attrs'];

  const directive = {
    link: {
      post: clickOutsideLinkPost,
    },
  };

  return directive;
}

clickOutside.$inject = ['$document'];

module.exports = clickOutside;
