/* eslint prefer-template: 0, no-console: 0 */

class Api {

  constructor(options) {
    this.settings = {
      apiDomain: 'http://fin/',
      apiPath: 'manage/api/',
      credentials: 'omit',
      version: '2',
      onUncauchError(error) {
        console.error(error);
      },
    };

    this.methods = {
      GET_GAMES: 'games.get',
      GET_SCENARIOS: 'scenarios.get',
      GET_TEAMS: 'games.getTeams',
      GET_USERS: 'users.get',
      FINISH_GAME: 'games.finish',
      CHANGE_GAME_DESCRIPTION: 'games.changeDescription',
      CHANGE_GAME_NAME: 'games.changeName',
      CREATE_GAME: 'games.create',
      CREATE_TEAM_GAME: 'games.createTeam',
      DELETE_TEAM_GAME: 'games.deleteTeam',
      RESET_GAME: 'games.reset',
      CHANGE_MEMBERS_TEAM: 'games.changeTeamMembers',
      CHANGE_GAME_SCENARIO: 'games.changeScenario',
      CALCULATE_GAME: 'games.calc',
      CHANGE_GAME_MEMBERS: 'games.changeMembers',
      MODIFY_GAME_INTERFACE: 'games.interface',
      FORK_GAME: 'games.fork',
      DELETE_GAME: 'games.delete',
    };

    Object.assign(this.settings, options);
  }

  call(method, params) {
    let queryUrl = this.settings.apiDomain
                   + this.settings.apiPath
                   + method
                   + '?v=' + this.settings.version;
    _.forEach(params, (val, key) => {
      queryUrl += `&${key}=${val}`;
    });


    return fetch(queryUrl, {
      method: 'GET',
      credentials: this.settings.credentials,
    })
    .then(response => response.json())
    .then((json) => {
      if (({}).hasOwnProperty.call(json, 'message')
      && ({}).hasOwnProperty.call(json, 'type')
      && json.type === 'error') {
        this.settings.onUncauchError(json);
        return Promise.reject(json);
      }
      return json;
    });
  }

  getGames() {
    return this.call(this.methods.GET_GAMES);
  }

  getScenarios() {
    return this.call(this.methods.GET_SCENARIOS);
  }

  getTeams() {
    return this.call(this.methods.GET_TEAMS);
  }

  getUsers() {
    return this.call(this.methods.GET_USERS);
  }

  finishGame(id, finished = true) {
    return this.call(this.methods.FINISH_GAME, {
      status: finished ? 'finish' : 'resume',
      id,
    });
  }

  changeGameDescription(id, description = '') {
    return this.call(this.methods.CHANGE_GAME_DESCRIPTION, {
      id,
      description,
    });
  }

  changeGameName(id, name) {
    return this.call(this.methods.CHANGE_GAME_NAME, {
      id,
      name,
    });
  }

  createGame(name) {
    return this.call(this.methods.CREATE_GAME, { name });
  }

  createGameTeam(id) {
    return this.call(this.methods.CREATE_TEAM_GAME, { id });
  }

  deleteGameTeam(id, teamId) {
    return this.call(this.methods.DELETE_TEAM_GAME, {
      id,
      teamId,
    });
  }

  resetGame(id, keepScenario = false, keepTeams = false) {
    return this.call(this.methods.RESET_GAME, {
      id,
      keepScenario: Number(keepScenario),
      keepTeams: Number(keepTeams),
    });
  }

  changeTeamMembers(id, teamId, { add, remove }) {
    const req = { id, teamId };
    if (_.isArray(add) && add.length > 0) req.add = add.join(',');
    if (_.isArray(remove) && remove.length > 0) req.remove = remove.join(',');
    return this.call(this.methods.CHANGE_MEMBERS_TEAM, req);
  }

  changeGameScenario(id, scenarioId) {
    return this.call(this.methods.CHANGE_GAME_SCENARIO, {
      id,
      scenarioId,
    });
  }

  calculateGame(id, period) {
    return this.call(this.methods.CALCULATE_GAME, {
      id,
      period,
    });
  }

  changeGameMembers(id, { add, remove }) {
    const req = { id };
    if (_.isArray(add) && add.length > 0) req.add = add.join(',');
    if (_.isArray(remove) && remove.length > 0) req.remove = remove.join(',');
    return this.call(this.methods.CHANGE_GAME_MEMBERS, req);
  }

  modifyGameInterface(id, status) {
    return this.call(this.methods.MODIFY_GAME_INTERFACE, {
      id,
      status: status ? 'unlock' : 'lock',
    });
  }

  forkGame(id) {
    return this.call(this.methods.FORK_GAME, { id });
  }

  deleteGame(id) {
    return this.call(this.methods.DELETE_GAME, { id });
  }
}

module.exports = Api;
